<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 13/5/19
 * Time: 4:18 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog\Seeders;

class BlogSeeder
{
    public function run()
    {
         $this->seedCategory();
    }

    //seed uncategorized category
    protected function seedCategory()
    {
        \DB::table('blog_category')->truncate();
        \DB::table('blog_category')->insert([
            'id'=>1,
            'category_name'=>'uncategorized',
            'category_slug'=>'uncategorized'
        ]);
    }
}