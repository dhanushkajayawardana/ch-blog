@extends('Admin.layout')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @if (isset($model))
                        @include('Admin.partials.breadcumbs',['header' => $model->first_name,'params' => $model])
                    @else
                        @include('Admin.partials.breadcumbs',['header'=>__('general.Create Author')])
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if (isset($model))
                            <h4 class="card-title" id="basic-layout-form-center">{{__('authors.Update Author')}}</h4>
                        @else
                            <h4 class="card-title" id="basic-layout-form-center">{{__('authors.Create new Author')}}</h4>
                        @endif
                    </div>
                    <div class="card-content show">
                        <div class="card-body">
                            @if(isset($model))
                            <form action="{{ route('admin.authors.update',['author'=>$model->id]) }}" method="POST"
                                  class="form-horizontal" enctype="multipart/form-data">
                                @else
                                    <form action="{{route('admin.authors.store')}}" method="POST"
                                          class="form-horizontal" enctype="multipart/form-data">
                                @endif
                                {{ csrf_field() }}

                                @if (isset($model))

                                    <input type="hidden" name="_method" value="PATCH">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="{{$model->id}}">
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('authors.First Name')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="first_name" id="first_name" class="form-control" value="{{isset($model) ? $model->first_name : (old('first_name'))?? '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-sm-3 control-label">{{__('authors.Last Name')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="last_name" id="last_name" class="form-control" value="{{isset($model) ? $model->last_name : (old('last_name'))?? ''  }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('authors.Email')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="email" name="email" id="email" class="form-control" value="{{isset($model) ? $model->email : (old('email'))?? ''  }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="avatar" class="col-sm-3 control-label">{{__('authors.Avatar')}}</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" class="form-control"
                                               name="avatar">

                                        @if(isset($model))
                                            <input  type="hidden" class="form-control" name="featured_image_old" value="{{$model->avatar}}">
                                            <img id="image_preview" src="{{asset($model->avatar)}}" alt="featured" class="height-150 img-thumbnail">
                                        @else
                                            <img id="image_preview" class="hide height-150 img-thumbnail" >
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rank" class="col-sm-3 control-label">{{__('authors.Position')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="rank" id="rank" class="form-control" value="{{isset($model) ? $model->user_rank : (old('rank'))?? ''  }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rank" class="col-sm-3 control-label">{{__('authors.Notes')}}</label>
                                    <div class="col-sm-12">
                                        <textarea name="notes" id="notes" cols="30" rows="10" class="form-control">{{isset($model) ? $model->notes : (old('notes'))?? ''  }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rank" class="col-sm-3 control-label">{{__('authors.LinkedIn Profile')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="linkedin" id="linkedin" class="form-control" value="{{isset($model) && $model->socialProfiles ? $model->socialProfiles->where('social_media','linkedin')->first()->profile_link?? '' : (old('linkedin'))?? ''  }}">
                                    </div>
                                </div>
                                <div class="form-actions left">
                                    <button type="reset" class="btn btn-warning mr-1">
                                        <i class="ft-x"></i> {{__('trainings.general.reset')}}</button>

                                    <a href="{{ URL::previous() }}">
                                        <button type="button" href="" class="btn btn-warning mr-1">
                                            <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-primary" id="saveForm">
                                        <i class="fa fa-check-square-o"></i> {{__('trainings.general.update')}}
                                    </button>
                                    {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                        <i class="fa fa-check-square-o"></i> {{__('trainings.general.updateAndBack')}}
                                    </button>--}}
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        window.STARPATH="{{asset('images/raty/')}}";
    </script>
    <script src="{{ asset("vendors/js/extensions/jquery.raty.js")}}"></script>
    <script src="{{ asset("js/scripts/authors_scripts.js")}}"></script>
@endsection