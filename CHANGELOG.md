# Changelog

All notable changes to `ch-blog` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release


## 2.0.0
- moved logic into src
- extended controllers with core controllers
- publish only route,views,config 