<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    
    'fe_base_url' => env('FE_BASE_URL', ''),

    'fe_blog_url' => env('FE_BLOG_URL', ''),

    'imageoptimaization' => [

        'img_quality' => env('CH_BLOG_IMG_QUALITY', 90),

        'img_format' => env('CH_BLOG_IMG_FORMAT', 'webp'),


        'featured_image' => [

            ['width' => 400, 'height' => 200], // admin_article_list

            ['width' => 200, 'height' => 167], // front_article_list

            ['width' => 200, 'height' => 150]  // article_thumbnail
        ],

        'fearured_image_default_size' => '400x200',


    ]
];